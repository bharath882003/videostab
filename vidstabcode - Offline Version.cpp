//Processing Time: 21.084
//Processing Speed for 908 frames: 43.065s

#include <opencv2/opencv.hpp>
#include <iostream>
#include <cassert>
#include <cmath>
#include <fstream>
#include <ctime>

using namespace std;
using namespace cv;

// This video stablisation smooths the global trajectory using a sliding average window
#define SWITCHING_FRAME 20
#define RIGID_MECHANISM 0
#define AFFINE_MECHANISM 1
#define RIGID_SCALE 1

#define CORNER_THRESHOLD 10

#define MAX_CORNERS 200
#define CORNER_QUALITY 0.07
#define MINIMUM_DISTANCE 10

const int SMOOTHING_RADIUS = 5; // In frames. The larger the more stable the video, but less reactive to sudden panning
const int HORIZONTAL_BORDER_CROP = 20; // In pixels. Crops the border to reduce the black borders from stabilisation being too noticeable.
const string VIDEO_NAME = "hippo.mp4";
const string OUTPUT_VIDEO_NAME = "Best_Output.avi";
//const string OUTPUT_VIDEO_NAME = "compare(Switching Mechanism + Scene Change).avi";

// 1. Get previous to current frame transformation (dx, dy, da) for all frames
//	a.		// translation + rotation only
//T = transformation matrix 3x3: 
//T.at(0,2) = tx, T.at(1,2) = ty
//T.at (0,0) = s cos0 T.at(0,1) = -s sin0
//T.at(1,0) = s sin0 T.at(1,1) = s cos0
//where s is the scaling factor
// 2. Accumulate the transformations to get the image trajectory
// 3. Smooth out the trajectory using an averaging window
// 4. Generate new set of previous to current transform, such that the trajectory ends up being the same as the smoothed trajectory
// 5. Apply the new transformation to the video

//ROI area (done)
//scaling of new transformation (done)
//last known good T initialise for first frame 
//if not stabilizing frame, use last known good T instead of identity matrix

struct TransformParam
{
	TransformParam() {}
	TransformParam(double _dx, double _dy, double _da, double _ds) {
		dx = _dx;
		dy = _dy;
		da = _da; // angle
		ds = _ds; //scaling factor
	}

	double dx;
	double dy;
	double da; // angle
	double ds;
};

struct TrajectoryAffine
{
	TrajectoryAffine() {}
	TrajectoryAffine(double _x, double _y, double _a, double _s) {
		x = _x;
		y = _y;
		a = _a;
		s = _s;
	}

	double x;
	double y;
	double a; // angle
	double s; //scaling factor
};

//Every 20 Frames the swtiching mechanism will be called 
int RigidorAffine(Mat Rigid, Mat partialAffine, vector<Point2f> prev_corner2){

	vector<Point2f> newPointsRigid;
	vector<Point2f> newPointspartialAffine;

	//
	for (int i = 0; i < prev_corner2.size(); i++){
		double _xR = Rigid.at<double>(0, 0) * prev_corner2[i].x + Rigid.at<double>(0, 1) * prev_corner2[i].y + Rigid.at<double>(0, 2);
		double _yR = Rigid.at<double>(1, 0) * prev_corner2[i].x + Rigid.at<double>(1, 1) * prev_corner2[i].y + Rigid.at<double>(1, 2);
		double _wR = Rigid.at<double>(2, 0) * prev_corner2[i].x + Rigid.at<double>(2, 1) * prev_corner2[i].y + 1;
		double _xA = partialAffine.at<double>(0, 0) * prev_corner2[i].x + partialAffine.at<double>(0, 1) * prev_corner2[i].y + partialAffine.at<double>(0, 2);
		double _yA = partialAffine.at<double>(1, 0) * prev_corner2[i].x + partialAffine.at<double>(1, 1) * prev_corner2[i].y + partialAffine.at<double>(1, 2);
		double _wA = partialAffine.at<double>(2, 0) * prev_corner2[i].x + partialAffine.at<double>(2, 1) * prev_corner2[i].y + 1;
		//changing to homogenous (real coordinates of corners in current frame

		double xR = _xR / _wR;
		double yR = _yR / _wR;
		double xA = _xA / _wA;
		double yA = _yA / _wA;
		Point2f ptR(xR, yR);
		Point2f ptA(xA, yA);
		newPointsRigid.push_back(ptR);
		newPointspartialAffine.push_back(ptA);
	}

	//Comparing
	double avgDiffRigid = 0;
	double avgDiffAffine = 0;
	for (int i = 0; i < prev_corner2.size(); i++){
		avgDiffRigid += sqrt((pow((prev_corner2[i].x - newPointsRigid[i].x), 2) + pow((prev_corner2[i].y - newPointsRigid[i].y), 2)));
		avgDiffAffine += sqrt((pow((prev_corner2[i].x - newPointspartialAffine[i].x), 2) + pow((prev_corner2[i].y - newPointspartialAffine[i].y), 2)));
	}

	avgDiffRigid /= prev_corner2.size();
	avgDiffAffine /= prev_corner2.size();

	//return Finding
	if (avgDiffRigid > avgDiffAffine){
		return AFFINE_MECHANISM;
	}
	else if (avgDiffRigid < avgDiffAffine){
		return RIGID_MECHANISM;
	}
	else
		RIGID_MECHANISM;
}

void featurePointFilter(vector<Point2f> GFPts, vector<Point2f> OFPts, vector<Point2f> &Results){
	for (int i = 0; i < OFPts.size(); i++){
		for (int j = 0; j < GFPts.size(); j++){
			double x = OFPts[i].x - GFPts[j].x;
			double y = OFPts[i].y - GFPts[j].y;
			if ((-7 < x  &&  x < 7) && (-5 < y && y < 5)){
				//cout << "Point found!" << endl;
				Results.push_back(OFPts[i]);
				i++;
				j = 0;
			}
		}
	}
}


int main(int argc, char **argv)
{
	//vector<Point2f> persistantPts[CORNER_POINTS_PERSISTANT_FRAMES];
	//vector<Point2f> existingPoints;

	//vector <int> trackPointNumber;
	// For further analysis
	ofstream out_transform("prev_to_cur_transformation.txt");
	ofstream out_TrajectoryRigid("TrajectoryRigid.txt");
	ofstream out_smoothed_TrajectoryRigid("smoothed_TrajectoryRigid.txt");
	ofstream out_new_transform("new_prev_to_cur_transformation.txt");

	Mat cur, cur_grey;
	Mat prev, prev_grey;
	Mat I = (Mat_<double>(3, 3) << (1, 0, 0, 0, 1, 0, 0, 0, 1));
	Mat last_T = I;

	// Vectors to store feature points
	vector <Point2f> prev_corner, cur_corner;
	vector <Point2f> prev_corner2, cur_corner2;
	// Step 1 - Get previous to current frame transformation (dx, dy, da) for all frames
	vector <TransformParam> prev_to_cur_transform;

	//vector <TransformParamRigid> prev_to_cur_transformRigid; // previous to current

	int k = 1;
	int countRigidTransform = 0, countAffineTransform = 0, counter = 0;
	enum transformationMethod { RIGID, AFFINE };
	transformationMethod currentMethod = RIGID;

	vector <uchar> status;
	vector <float> err;

	VideoCapture cap(VIDEO_NAME);
	assert(cap.isOpened());
	cap >> prev;
	cvtColor(prev, prev_grey, COLOR_BGR2GRAY);
	//Gets the maximum number of frames
	int max_frames = cap.get(CV_CAP_PROP_FRAME_COUNT);

	while (true) {
		Mat R, pA, T;
		vector<Point2f> filteredResult;

		cap >> cur;
		if (cur.data == NULL) {
			break;
		}
		cvtColor(cur, cur_grey, COLOR_BGR2GRAY);

		//Removes good features to track as we already pre-defined the corners to be 20 pixels in
		Rect ROI(20, 20, prev_grey.cols - 40, prev_grey.rows - 40);

		Mat prev_grey_roi = prev_grey(ROI);

		//Initiates the feature points tracking
		/*if (k == 1){
			goodFeaturesToTrack(prev_grey, prev_corner, MAX_CORNERS, CORNER_QUALITY, MINIMUM_DISTANCE);
		}*/
		if (k % 1 == 0){
			goodFeaturesToTrack(prev_grey, prev_corner, MAX_CORNERS, CORNER_QUALITY, MINIMUM_DISTANCE);
		}
		//If above certain threshold then do OF calculation, if not just insert identity matrix into transformParams vector indicating no change.
		if (prev_corner.size() > CORNER_THRESHOLD){
			calcOpticalFlowPyrLK(prev_grey, cur_grey, prev_corner, cur_corner, status, err);

			// weed out bad matches 
			for (size_t i = 0; i < status.size(); i++) {
				if (status[i]) {
					prev_corner2.push_back(prev_corner[i]);
					cur_corner2.push_back(cur_corner[i]);
				}
			}
			// Deciding Transformation	
			try{
				R = estimateRigidTransform(prev_corner2, cur_corner2, false);
				pA = estimateRigidTransform(prev_corner2, cur_corner2, true);
				prev_corner.clear();
				for (int i = 0; i < cur_corner2.size(); i++){
					prev_corner.push_back(cur_corner2[i]);
				}
				counter++;
			}
			catch (...){
				R = I;
				pA = I;
				currentMethod = RIGID;
				prev_corner.clear();
				goodFeaturesToTrack(cur_grey, prev_corner, MAX_CORNERS, CORNER_QUALITY, MINIMUM_DISTANCE);
				cout << "R or pA is invalid" << endl;
			}

			if (k % SWITCHING_FRAME == 0){
				if (!R.empty() && !pA.empty() && RigidorAffine(R, pA, prev_corner2) == 1){
					currentMethod = AFFINE;
					countAffineTransform++;
				}
				else{
					currentMethod = RIGID;
					countRigidTransform++;
				}
			}
			if (currentMethod == RIGID){
				T = R; // false = rigid transform, no scaling/shearing
			}
			else if (currentMethod == AFFINE){
				T = pA;
			}

			// in rare cases no transform is found. We'll just use the last known good transform.
			if (T.data == NULL) {
				last_T.copyTo(T);
			}

			// decompose T
			double dx = T.at<double>(0, 2);
			double dy = T.at<double>(1, 2);
			double da = atan2(T.at<double>(1, 0), T.at<double>(0, 0)); //inverse tan to find 0
			//Check Outliers for dx, dy and da;
			if (dx < -100 || dx > 100){
				dx = 0;
			}
			else
				T.copyTo(last_T);
			if (dy < -100 || dy > 100){
				dy = 0;
			}
			else
				T.copyTo(last_T);
			if (da > 0.7853 || da < -0.7853){
				da = 0;
				cout << "da: " << da << endl;
			}
			else
				T.copyTo(last_T);


			if (currentMethod == RIGID){
				prev_to_cur_transform.push_back(TransformParam(dx, dy, da, RIGID_SCALE));
				out_transform << "Frame:" << k << " dx:" << dx << " dy:" << dy << " da:" << da << " ds:" << RIGID_SCALE << endl;
			}
			else if (currentMethod == AFFINE){
				double ds = T.at<double>(0, 0) / cos(da);
				prev_to_cur_transform.push_back(TransformParam(dx, dy, da, ds));
				out_transform << "Frame:" << k << " dx:" << dx << " dy:" << dy << " da:" << da << " ds:" << ds << endl;
			}

		}
		else{
			//Don't stabilize that frame
			prev_to_cur_transform.push_back(TransformParam(0, 0, 0, 1));

			out_transform << "Frame:" << k << " dx:" << 0 << " dy:" << 0 << " da:" << 0 << " ds:" << 1 << endl;
			if (!prev_corner.empty())
				prev_corner.clear();
			goodFeaturesToTrack(cur_grey, prev_corner, MAX_CORNERS, CORNER_QUALITY, MINIMUM_DISTANCE);
		}

		//Filtering the corners here
		goodFeaturesToTrack(cur_grey, cur_corner, MAX_CORNERS, CORNER_QUALITY, MINIMUM_DISTANCE);
		featurePointFilter(prev_corner, cur_corner, filteredResult);
		prev_corner.clear();
		for (int i = 0; i < filteredResult.size(); i++){
			prev_corner.push_back(filteredResult[i]);
		}

		cur.copyTo(prev);
		cur_grey.copyTo(prev_grey);
		cout << "Frame: " << k << "/" << max_frames << " - good optical flow: " << prev_corner2.size() << endl;
		k++;

		if (!prev_corner2.empty())
			prev_corner2.clear();
		if (!cur_corner2.empty())
			cur_corner2.clear();

	}

	cout << "Number of times RIGID transform takes place(per 20 frames): " << countRigidTransform << endl;
	cout << "Number of times AFFFINE transform takes place(per 20 frames): " << countAffineTransform << endl;
	cout << "counter: " << counter << endl;

	// Step 2 - Accumulate the transformations to get the image trajectory

	// Accumulated frame to frame transform
	double a = 0;
	double x = 0;
	double y = 0;
	double s = 0;

	vector <TrajectoryAffine> Trajectory; // Trajectory at all frames

	for (size_t i = 0; i < prev_to_cur_transform.size(); i++) {
		/*if (i == Trajectory.size() - 1){
			for (int j = 0; j < SMOOTHING_RADIUS; j++)
			s += prev_to_cur_transform[i].ds;
			}*/
		x += prev_to_cur_transform[i].dx;
		y += prev_to_cur_transform[i].dy;
		a += prev_to_cur_transform[i].da;
		s += prev_to_cur_transform[i].ds;

		Trajectory.push_back(TrajectoryAffine(x, y, a, s));

		out_TrajectoryRigid << "Frame:" << (i + 1) << " x:" << x << endl << " y:" << y << endl << " a:" << a << endl << " s:" << s << endl;
	}
	cout << "Trajectory size: " << Trajectory.size() << endl;
	// Step 3 - Smooth out the trajectory using an averaging window
	vector <TrajectoryAffine> smoothed_Trajectory; // trajectory at all frames

	for (size_t i = 0; i < Trajectory.size(); i++) {
		double sum_x = 0;
		double sum_y = 0;
		double sum_a = 0;
		double sum_s = 0;
		int count = 0;

		for (int j = -SMOOTHING_RADIUS; j <= SMOOTHING_RADIUS; j++) {

			if (i + j >= 0 && i + j < Trajectory.size()) {
				sum_x += Trajectory[i + j].x;
				sum_y += Trajectory[i + j].y;
				sum_a += Trajectory[i + j].a;
				sum_s += Trajectory[i + j].s;

				count++;
			}
		}

		double avg_a = sum_a / count;
		double avg_x = sum_x / count;
		double avg_y = sum_y / count;
		double avg_s = sum_s / count;

		smoothed_Trajectory.push_back(TrajectoryAffine(avg_x, avg_y, avg_a, avg_s));

		out_smoothed_TrajectoryRigid << "Frame:" << (i + 1) << " avg_x:" << avg_x << " avg_y:" << avg_y << " avg_a:" << avg_a << " avg_s: " << avg_s << endl;
	}
	cout << "Smoothed Trajectory size: " << smoothed_Trajectory.size() << endl;

	// Step 4 - Generate new set of previous to current transform, such that the trajectory ends up being the same as the smoothed trajectory
	vector <TransformParam> new_prev_to_cur_transform;

	// Accumulated frame to frame transform
	a = 0;
	x = 0;
	y = 0;
	s = 0;

	for (size_t i = 0; i < prev_to_cur_transform.size(); i++) {

		// target - current
		double diff_x = smoothed_Trajectory[i].x - Trajectory[i].x;
		double diff_y = smoothed_Trajectory[i].y - Trajectory[i].y;
		double diff_a = smoothed_Trajectory[i].a - Trajectory[i].a;
		double diff_s = smoothed_Trajectory[i].s - Trajectory[i].s;

		double dx = prev_to_cur_transform[i].dx + diff_x;
		double dy = prev_to_cur_transform[i].dy + diff_y;
		double da = prev_to_cur_transform[i].da + diff_a;
		double ds = prev_to_cur_transform[i].ds + diff_s;

		//Check for outliers after smoothing
		if (ds < 0.9 || ds > 1.4)
			ds = 1;

		new_prev_to_cur_transform.push_back(TransformParam(dx, dy, da, ds));

		out_new_transform << "Frame:" << (i + 1) << " dx:" << dx << " dy:" << dy << " da:" << da << " ds:" << ds << endl;
	}

	// Step 5 - Apply the new transformation to the video
	cap.set(CV_CAP_PROP_POS_FRAMES, 0);
	Mat T(2, 3, CV_64F);


	int vert_border = HORIZONTAL_BORDER_CROP * prev.rows / prev.cols; // get the aspect ratio correct

	k = 0;

	VideoWriter result;

	Mat frameWriteSize = Mat::zeros(cur.rows, cur.cols * 2 + 10, cur.type());
	result.open(OUTPUT_VIDEO_NAME, 1, 30, frameWriteSize.size(), true);
	if (result.isOpened())

	while (k < max_frames - 1) { // don't process the very last frame, no valid transform
		cap >> cur;

		if (cur.data == NULL) {
			break;
		}
		/*T.at<double>(2, 0) = 0;
		T.at<double>(2, 1) = 0;
		T.at<double>(2, 2) = 1;*/
		if (new_prev_to_cur_transform[k].ds != RIGID_SCALE){
			T.at<double>(0, 0) = new_prev_to_cur_transform[k].ds * cos(new_prev_to_cur_transform[k].da);
			T.at<double>(0, 1) = new_prev_to_cur_transform[k].ds * -sin(new_prev_to_cur_transform[k].da);
			T.at<double>(1, 0) = new_prev_to_cur_transform[k].ds * sin(new_prev_to_cur_transform[k].da);
			T.at<double>(1, 1) = new_prev_to_cur_transform[k].ds * cos(new_prev_to_cur_transform[k].da);
		}
		else{
			T.at<double>(0, 0) = cos(new_prev_to_cur_transform[k].da);
			T.at<double>(0, 1) = -sin(new_prev_to_cur_transform[k].da);
			T.at<double>(1, 0) = sin(new_prev_to_cur_transform[k].da);
			T.at<double>(1, 1) = cos(new_prev_to_cur_transform[k].da);
		}

		T.at<double>(0, 2) = new_prev_to_cur_transform[k].dx;
		T.at<double>(1, 2) = new_prev_to_cur_transform[k].dy;

		Mat cur2;

		warpAffine(cur, cur2, T, cur.size());

		cur2 = cur2(Range(vert_border, cur2.rows - vert_border), Range(HORIZONTAL_BORDER_CROP, cur2.cols - HORIZONTAL_BORDER_CROP));

		// Resize cur2 back to cur size, for better side by side comparison
		resize(cur2, cur2, cur.size());

		// Now draw the original and stablised side by side for coolness
		Mat canvas = Mat::zeros(cur.rows, cur.cols * 2 + 10, cur.type());

		cur.copyTo(canvas(Range::all(), Range(0, cur2.cols)));
		cur2.copyTo(canvas(Range::all(), Range(cur2.cols + 10, cur2.cols * 2 + 10)));

		// If too big to fit on the screen, then scale it down by 2, hopefully it'll fit :)
		if (canvas.cols > 1920) {
			resize(canvas, canvas, Size(canvas.cols / 2, canvas.rows / 2));
		}

		/*imshow("before and after", canvas);
		waitKey(30);*/
		result << canvas;
		k++;
		cout << "Frame: " << k << endl;
	}

	result.release();
	cap.release();
	exit(0);
	return 0;
}