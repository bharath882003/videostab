// This video stablisation smooths the global trajectory using a sliding average window
// 1. Get previous to current frame transformation (dx, dy, da) for all frames
//	a.		// translation + rotation only
//T = transformation matrix 3x3: 
//T.at(0,2) = tx, T.at(1,2) = ty
//T.at (0,0) = s cos0 T.at(0,1) = -s sin0
//T.at(1,0) = s sin0 T.at(1,1) = s cos0
//where s is the scaling factor
// 2. Accumulate the transformations to get the image trajectory
// 3. Smooth out the trajectory using an averaging window
// 4. Generate new set of previous to current transform, such that the trajectory ends up being the same as the smoothed trajectory
// 5. Apply the new transformation to the video
//
//ROI area (done)
//scaling of new transformation (done)

#include <opencv2/opencv.hpp>
#include <iostream>
#include <cassert>
#include <cmath>
#include <fstream>
#include <thread>
#include <mutex>
#include <deque>
#include <condition_variable>


#include <string.h>

using namespace std;
using namespace cv;

std::mutex _mu1; // Key for Transform Parameters
std::mutex _mu2; // Key for New Transform Parameters

#define FRAMES_PER_SECOND 30
#define SWITCHING_FRAME 20
#define RIGID_MECHANISM 0
#define AFFINE_MECHANISM 1
#define RIGID_SCALE 1
	
#define CORNER_THRESHOLD 10 //Initial 10
#define CORNER_RETRACK_FRAMES 20

#define MAX_CORNERS 100
#define CORNER_QUALITY 0.01
#define MINIMUM_DISTANCE 20

const int SMOOTHING_RADIUS = 10; // In frames. The larger the more stable the video, but less reactive to sudden panning
const int HORIZONTAL_BORDER_CROP = 20; // In pixels. Crops the border to reduce the black borders from stabilisation being too noticeable.
//const string VIDEO_NAME = "test.mp4";
struct TransformParam
{
	TransformParam() {}
	TransformParam(double _dx, double _dy, double _da, double _ds) {
		dx = _dx;
		dy = _dy;
		da = _da; // angle
		ds = _ds; //scaling factor
	}

	double dx;
	double dy;
	double da; // angle
	double ds;
};

//GLOBAL Variables 
VideoCapture cap, cap2;
static deque<TransformParam> prev_to_cur_transform; //Buffer for thread1 and thread2 
static deque<TransformParam> new_prev_to_cur_transform;
static int max_frames;

ofstream out_transform("prev_to_cur_transformation.txt");
ofstream out_Trajectory("trajectory.txt");
ofstream out_smoothed_TrajectoryRigid("smoothed_trajectory.txt");
ofstream out_new_transform("new_prev_to_cur_transformation.txt");

//Every 20 Frames the swtiching mechanism will be called 
int RigidorAffine(Mat Rigid, Mat partialAffine, vector<Point2f> prev_corner2){

	vector<Point2f> newPointsRigid;
	vector<Point2f> newPointspartialAffine;

	//
	for (int i = 0; i < prev_corner2.size(); i++){
		double _xR = Rigid.at<double>(0, 0) * prev_corner2[i].x + Rigid.at<double>(0, 1) * prev_corner2[i].y + Rigid.at<double>(0, 2);
		double _yR = Rigid.at<double>(1, 0) * prev_corner2[i].x + Rigid.at<double>(1, 1) * prev_corner2[i].y + Rigid.at<double>(1, 2);
		double _wR = Rigid.at<double>(2, 0) * prev_corner2[i].x + Rigid.at<double>(2, 1) * prev_corner2[i].y + 1;
		double _xA = partialAffine.at<double>(0, 0) * prev_corner2[i].x + partialAffine.at<double>(0, 1) * prev_corner2[i].y + partialAffine.at<double>(0, 2);
		double _yA = partialAffine.at<double>(1, 0) * prev_corner2[i].x + partialAffine.at<double>(1, 1) * prev_corner2[i].y + partialAffine.at<double>(1, 2);
		double _wA = partialAffine.at<double>(2, 0) * prev_corner2[i].x + partialAffine.at<double>(2, 1) * prev_corner2[i].y + 1;
		//changing to homogenous (real coordinates of corners in current frame

		double xR = _xR / _wR;
		double yR = _yR / _wR;
		double xA = _xA / _wA;
		double yA = _yA / _wA;
		Point2f ptR(xR, yR);
		Point2f ptA(xA, yA);
		newPointsRigid.push_back(ptR);
		newPointspartialAffine.push_back(ptA);
	}

	//Comparing
	double avgDiffRigid = 0;
	double avgDiffAffine = 0;
	for (int i = 0; i < prev_corner2.size(); i++){
		avgDiffRigid += sqrt((pow((prev_corner2[i].x - newPointsRigid[i].x), 2) + pow((prev_corner2[i].y - newPointsRigid[i].y), 2)));
		avgDiffAffine += sqrt((pow((prev_corner2[i].x - newPointspartialAffine[i].x), 2) + pow((prev_corner2[i].y - newPointspartialAffine[i].y), 2)));
	}

	avgDiffRigid /= prev_corner2.size();
	avgDiffAffine /= prev_corner2.size();

	//return Finding
	if (avgDiffRigid > avgDiffAffine){
		return AFFINE_MECHANISM;
	}
	else if (avgDiffRigid < avgDiffAffine){
		return RIGID_MECHANISM;
	}
	else
		RIGID_MECHANISM;
}

void motionEstimation(){

	/*ofstream out_transform("prev_to_cur_transformation.txt");*/

	Mat prev, prev_grey;
	Mat cur, cur_grey;
	Mat I = (Mat_<double>(3, 3) << (1, 0, 0, 0, 1, 0, 0, 0, 1));
	Mat last_T = I;

	/*vector <TransformParam> prev_to_cur_transform;*/
	vector <Point2f> prev_corner, cur_corner; // For motionEstimation
	vector <Point2f> prev_corner2, cur_corner2;
	vector <uchar> status;
	vector <float> err;

	int k = 1;
	int countRigidTransform = 0, countAffineTransform = 0, counter = 0;
	enum transformationMethod { RIGID, AFFINE };
	transformationMethod currentMethod = RIGID;

	cap >> prev;
	cvtColor(prev, prev_grey, COLOR_BGR2GRAY);
	//Gets the maximum number of frames
	
	while (k != max_frames) {

		Mat R, pA, T;

		cap >> cur;
		cvtColor(cur, cur_grey, COLOR_BGR2GRAY);
		(cur_grey, cur_grey, cv::Size(320, 240));
		/*imshow("", cur_grey);
		cv::waitKey();*/
		//Removes good features to track as we already pre-defined the corners to be 20 pixels in
		Rect ROI(10, 10, prev_grey.cols - 20, prev_grey.rows - 20);
		//Rect ROI(10, 10, prev_grey.cols - prev_grey.cols / 10 , prev_grey.rows - prev_grey.rows /10);

		Mat prev_grey_roi = prev_grey(ROI);
		Mat cur_grey_roi = cur_grey(ROI);

		//Initiates the feature points tracking
		if (k % CORNER_RETRACK_FRAMES == 0){
			goodFeaturesToTrack(prev_grey_roi, prev_corner, MAX_CORNERS, CORNER_QUALITY, MINIMUM_DISTANCE);
		}
		//goodFeaturesToTrack(prev_grey_roi, prev_corner, MAX_CORNERS, CORNER_QUALITY, MINIMUM_DISTANCE);
		//If above certain threshold then do OF calculation, if not just insert identity matrix into transformParams vector indicating no change.
		if (prev_corner.size() > CORNER_THRESHOLD){
			calcOpticalFlowPyrLK(prev_grey, cur_grey, prev_corner, cur_corner, status, err);

			// weed out bad matches 
			for (size_t i = 0; i < status.size(); i++) {
				if (status[i]) {
					prev_corner2.push_back(prev_corner[i]);
					cur_corner2.push_back(cur_corner[i]);
				}
			}
			// Deciding Transformation	
			try{
				if (currentMethod == RIGID)
					R = estimateRigidTransform(prev_corner2, cur_corner2, false);
				else if (currentMethod == AFFINE)
					pA = estimateRigidTransform(prev_corner2, cur_corner2, true);

				prev_corner.clear();
				for (int i = 0; i < cur_corner2.size(); i++){
					prev_corner.push_back(cur_corner2[i]);
				}
				counter++;
			}
			catch (...){
				R = I;
				pA = I;
				currentMethod = RIGID;
				prev_corner.clear();
				goodFeaturesToTrack(cur_grey_roi , prev_corner, MAX_CORNERS, CORNER_QUALITY, MINIMUM_DISTANCE);
				cout << "R or pA is invalid" << endl;
			}
			/*cout << "Debug Point 2! " << endl;*/
			if (k % SWITCHING_FRAME == 0){
				R = estimateRigidTransform(prev_corner2, cur_corner2, false);
				pA = estimateRigidTransform(prev_corner2, cur_corner2, true);
				if (!R.empty() && !pA.empty() && RigidorAffine(R, pA, prev_corner2) == 1){
					currentMethod = AFFINE;
					countAffineTransform++;
				}
				else{
					currentMethod = RIGID;
					countRigidTransform++;
				}
			}
			if (currentMethod == RIGID){
				T = R; // false = rigid transform, no scaling/shearing
			}
			else if (currentMethod == AFFINE){
				T = pA;
			}

			// in rare cases no transform is found. We'll just use the last known good transform.
			if (T.data == NULL) {
				last_T.copyTo(T);
			}

			// decompose T
			double dx = T.at<double>(0, 2);
			double dy = T.at<double>(1, 2);
			double da = atan2(T.at<double>(1, 0), T.at<double>(0, 0)); //inverse tan to find 0
			//Check Outliers for dx, dy and da;
			if (dx < -100 || dx > 100){
				dx = 0;
			}
			else
				last_T.at<double>(0, 2) = T.at<double>(0, 2);
			if (dy < -100 || dy > 100){
				dy = 0;
			}
			else
				last_T.at<double>(1, 2) = T.at<double>(1, 2);
			if (da > 0.7853 || da < -0.7853){
				da = 0;
			}
			else
				T.copyTo(last_T);


			if (currentMethod == RIGID){
				std::unique_lock<mutex> _keyToTF(_mu1);
				prev_to_cur_transform.push_back(TransformParam(dx, dy, da, RIGID_SCALE));
				_keyToTF.unlock();
				out_transform << "Frame:" << k << " dx:" << dx << " dy:" << dy << " da:" << da << " ds:" << RIGID_SCALE << endl;
			}
			else if (currentMethod == AFFINE){
				double ds = T.at<double>(0, 0) / cos(da);
				std::unique_lock<mutex> _keyToTF(_mu1);
				prev_to_cur_transform.push_back(TransformParam(dx, dy, da, ds));
				_keyToTF.unlock();
				out_transform << "Frame:" << k << " dx:" << dx << " dy:" << dy << " da:" << da << " ds:" << ds << endl;
			}

		}
		else{
			//Don't stabilize that frame
			std::unique_lock<mutex> _keyToTF(_mu1);
			prev_to_cur_transform.push_back(TransformParam(0, 0, 0, 1));
			_keyToTF.unlock();
			out_transform << "Frame:" << k << " dx:" << 0 << " dy:" << 0 << " da:" << 0 << " ds:" << 1 << endl;
			if (!prev_corner.empty())
				prev_corner.clear();
			goodFeaturesToTrack(cur_grey_roi, prev_corner, MAX_CORNERS, CORNER_QUALITY, MINIMUM_DISTANCE);
		}
		cur.copyTo(prev);
		cur_grey.copyTo(prev_grey);
		cout << "Frame: " << k << "/" << max_frames << " - good optical flow: " << prev_corner2.size() << endl;
		k++;

		if (!prev_corner2.empty())
			prev_corner2.clear();
		if (!cur_corner2.empty())
			cur_corner2.clear();
	}
	cout << "Number of times RIGID transform takes place(per 20 frames): " << countRigidTransform << endl;
	cout << "Number of times AFFFINE transform takes place(per 20 frames): " << countAffineTransform << endl;
	cap.release();
	/*exit(0);*/
	cin >> k;
}

void motionCompensation(){
	int k = 0;
	while (true){
		/*std::this_thread::sleep_for(std::chrono::milliseconds(5));*/
		if (prev_to_cur_transform.size() == SMOOTHING_RADIUS * 2){
			deque<TransformParam> Trajectory; // Trajectory at all frames
			deque<TransformParam> smoothed_Trajectory; // trajectory at all frames
			std::unique_lock<mutex> _keyToTF(_mu1); //Locks unprocessed parameters
			//Do averaging here.
			double a = 0;
			double x = 0;
			double y = 0;
			double s = 0;
			//Start averaging here

			for (size_t i = 0; i < SMOOTHING_RADIUS * 2; i++) {
				x += prev_to_cur_transform[i].dx;
				y += prev_to_cur_transform[i].dy;
				a += prev_to_cur_transform[i].da;
				s += prev_to_cur_transform[i].ds;

				Trajectory.push_back(TransformParam(x, y, a, s));

				out_Trajectory << "Frame:" << (i + 1) << " x:" << x << endl << " y:" << y << endl << " a:" << a << endl << " s:" << s << endl;
			}
			// Smooth out the trajectory using an averaging window
			for (size_t i = 0; i < Trajectory.size(); i++) {
				double sum_x = 0;
				double sum_y = 0;
				double sum_a = 0;
				double sum_s = 0;
				int count = 0;
				for (int j = -SMOOTHING_RADIUS; j < SMOOTHING_RADIUS; j++) {
					if (i + j >= 0 && i + j < Trajectory.size()) {
						sum_x += Trajectory[i + j].dx;
						sum_y += Trajectory[i + j].dy;
						sum_a += Trajectory[i + j].da;
						sum_s += Trajectory[i + j].ds;
						count++;
					}
				}

				double avg_a = sum_a / count;
				double avg_x = sum_x / count;
				double avg_y = sum_y / count;
				double avg_s = sum_s / count;
				smoothed_Trajectory.push_back(TransformParam(avg_x, avg_y, avg_a, avg_s));

				out_smoothed_TrajectoryRigid << "Frame:" << (i + 1) << " avg_x:" << avg_x << " avg_y:" << avg_y << " avg_a:" << avg_a << " avg_s: " << avg_s << endl;
			}

			// Accumulated frame to frame transform
			double da = 0;
			double dx = 0;
			double dy = 0;
			double ds = 0;

			for (size_t i = 0; i < SMOOTHING_RADIUS * 2; i++) {
				// target - current
				double diff_x = smoothed_Trajectory[i].dx - Trajectory[i].dx;
				double diff_y = smoothed_Trajectory[i].dy - Trajectory[i].dy;
				double diff_a = smoothed_Trajectory[i].da - Trajectory[i].da;
				double diff_s = smoothed_Trajectory[i].ds - Trajectory[i].ds;

				dx = prev_to_cur_transform[i].dx + diff_x;
				dy = prev_to_cur_transform[i].dy + diff_y;
				da = prev_to_cur_transform[i].da + diff_a;
				ds = prev_to_cur_transform[i].ds + diff_s;

				//Check for outliers after smoothing
				if (ds < 0.9 || ds > 1.2)
					ds = 1;
				k++;
				if (i == SMOOTHING_RADIUS)
					break;
			}
			std::unique_lock<mutex> _keyToNewTF(_mu2);
			new_prev_to_cur_transform.push_back(TransformParam(dx, dy, da, ds)); //Taking last frame pushed into the queue
			_keyToNewTF.unlock();
			out_new_transform << "Frame:" << k << " dx:" << dx << " dy:" << dy << " da:" << da << " ds:" << ds << endl;
			prev_to_cur_transform.pop_front();
			_keyToTF.unlock();
		}
	}
}

void display(){
	while (true){
		/*std::this_thread::sleep_for(std::chrono::milliseconds(20));*/
		if (!new_prev_to_cur_transform.empty()){
			Mat cur, cur2;
			TransformParam P;
			Mat T(2, 3, CV_64F);
			cap2 >> cur;
			int vert_border = HORIZONTAL_BORDER_CROP * cur.rows / cur.cols; // get the aspect ratio correct

			std::unique_lock<mutex> _keyToNewTP(_mu2);
			P = new_prev_to_cur_transform.front();
			new_prev_to_cur_transform.pop_front();
			_keyToNewTP.unlock();

			if (P.ds != RIGID_SCALE){
				T.at<double>(0, 0) = P.ds * cos(P.da);
				T.at<double>(0, 1) = P.ds * -sin(P.da);
				T.at<double>(1, 0) = P.ds * sin(P.da);
				T.at<double>(1, 1) = P.ds * cos(P.da);
			}
			else{
				T.at<double>(0, 0) = cos(P.da);
				T.at<double>(0, 1) = -sin(P.da);
				T.at<double>(1, 0) = sin(P.da);
				T.at<double>(1, 1) = cos(P.da);
			}

			T.at<double>(0, 2) = P.dx;
			T.at<double>(1, 2) = P.dy;

			warpAffine(cur, cur2, T, cur.size());

			cur2 = cur2(Range(vert_border, cur2.rows - vert_border), Range(HORIZONTAL_BORDER_CROP, cur2.cols - HORIZONTAL_BORDER_CROP));

			resize(cur2, cur2, cur.size());

			// Now draw the original and stablised side by side for coolness
			Mat canvas = Mat::zeros(cur.rows, cur.cols * 2 + 10, cur.type());

			cur.copyTo(canvas(Range::all(), Range(0, cur2.cols)));
			cur2.copyTo(canvas(Range::all(), Range(cur2.cols + 10, cur2.cols * 2 + 10)));

			// If too big to fit on the screen, then scale it down by 2, hopefully it'll fit :)
			if (canvas.cols > 1920) {
				resize(canvas, canvas, Size(canvas.cols / 2, canvas.rows / 2));
			}
			imshow("before and after", canvas);
			waitKey(30);

		}
	}
}

int main(int argc, char **argv)
{
	string _videoName;

	cout << "Please enter your video file name: " << endl;
	cout << "E.g. C:\\Videos\\test.avi " << endl << "**Note directory is unneccessary if .exe is in same folder: " << endl;
	cout << "Input Video File: ";
	cin >> _videoName;

	cap.open(_videoName);
	assert(cap.isOpened());

	cap2.open(_videoName);
	assert(cap2.isOpened());
	cap2.set(CV_CAP_PROP_POS_FRAMES, SMOOTHING_RADIUS);

	max_frames = cap.get(CV_CAP_PROP_FRAME_COUNT);
	thread thread1(motionEstimation); //Able to process homography between 
	thread thread2(motionCompensation);
	thread thread3(display); //Done able to display and get parameters from new_prev_to_cur_transform

	thread1.join();
	thread2.join();
	thread3.join();

	cap.release();
	cap2.release();

	return 0;
}