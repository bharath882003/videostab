***************************************************************************************************************

*Video Stabilization Setup and Execution
**Created by Bharath Ramesh and Lim Anli

***************************************************************************************************************
This README is for guiding people into setting up their PC/Machines into running Video Stabilization Code.

What the code does:
1. Takes in a video as input
2. Stabilize shaky motions in the video and display the smoothed result

***************************************************************************************************************

What you need:
1. OpenCV 2.4.9 installed
2. Microsoft Visual Studio 13 or higher (For Microsoft Visual Studio 15: *Remember to install C++ Toolkit)
3. Microsoft C++ Toolkit (should be included during installation of MSVisualStudio)

***************************************************************************************************************
OpenCV 2.4.9 Installation

1. Download OpenCV from www.opencv.org
2. Extract Opencv into desired folder (E.g. C:\opencv249)
3. Add environment variables, Right-Click PC -> Properties -> Advanced System Settings -> Environment Variables
4. Under System Variables -> New -> Variable: OPENCV_DIR, Value: C:\opencv249\build\x64\vc12
5. Under PATH -> Edit -> Append to the last entry -> Add -> ";%OPENCV_DIR%\bin", without quotes
6. Click Ok.
7. OpenCV is successfully installed.

***************************************************************************************************************
How to link Microsoft Visual Studio with OpenCV

1. Create a new project
2. Right-Click Project -> Select Properties
3. Configuration Manager -> Active Solution Configuration -> Debug -> Under Active Solution Platform -> <New> -> Select x64.
4. Do Step 3 for Release.
Now you have x64 configuration for both debug and release.

5. Under Configurations -> Select All Configurations + Under Platform -> Select x64
6. Under Configuration Properties -> Platform Toolset -> Visual Studio 2013(v120) should be selected
7. Under C/C++ Tab -> General -> Additional Include Directories -> Drop down list -> Edit -> New Line -> Add
   "$(OPENCV_DIR)\..\..\include"
8. Under Linker Tab -> General -> Additional Libraries Directories -> Add "$(OPENCV_DIR)\lib"
9. Under Linker Tab -> Input -> Additional Dependencies -> Add
opencv_core249.lib
opencv_highgui249.lib
opencv_imgproc249.lib
opencv_video249.lib
opencv_calib3d249.lib
10. Click Apply and Ok

***************************************************************************************************************

Code Execution

1. Add source code into source file -> Shift + Alt + A -> Select vidstabcode - Real-Time Version(Final)
2. Build source code -> Ctrl + Shift + B
3. Run -> Ctrl + F5

***************************************************************************************************************

